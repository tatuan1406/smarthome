//
//  ProfileViewController.swift
//  SmartHomeiOS
//
//  Created by Tinh Van on 4/18/19.
//  Copyright © 2019 hiup. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    fileprivate let profileHeaderCell = "profileHeaderCell"
    fileprivate let profileCell = "profileCell"
    
    let titleProfile = ["","Email","Phone","Address"]
    let detailProfile = ["","tatuanmac@gmail.com","(+84) 828 123 288","Cau Giay, Ha Noi"]
    let nameHeader = ["Ta Tuan"]
    let dateOfBirth = ["14-06-1996"]
    
    fileprivate  let profileContentView :UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    fileprivate let tableViewProfile : UITableView = {
        let tbl = UITableView()
        tbl.translatesAutoresizingMaskIntoConstraints = false
        tbl.backgroundColor = .clear
        return tbl
    }()
    
    fileprivate func profileSetup(width :CGFloat){
        
        view.backgroundColor = .black
        
        let layerYellow : UIView = {
            let layer = UIView()
            let gradient = CAGradientLayer()
            gradient.frame = CGRect(x: 0, y: 0, width: width, height: 2)
            gradient.colors = [
                UIColor(red: 0.97, green: 0.91, blue: 0.11, alpha: 0).cgColor,
                UIColor(red: 0.97, green: 0.91, blue: 0.11, alpha: 1).cgColor,
                UIColor(red: 0.97, green: 0.91, blue: 0.11, alpha: 0).cgColor
            ]
            gradient.locations = [0, 0.5239794075144508, 1]
            gradient.startPoint = CGPoint(x: 0.98, y: 0.52)
            gradient.endPoint = CGPoint(x: 0.02, y: 0.52)
            layer.layer.addSublayer(gradient)
            return layer
        }()
        
        let profileViews = ["profileContentView": profileContentView , "layerYellow": layerYellow , "tableViewProfile":tableViewProfile ]
        
        view.addSubview(profileContentView)
        profileContentView.addSubview(layerYellow)
        profileContentView.addSubview(tableViewProfile)
        
        //add constraint for view
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[profileContentView]|", options: [], metrics: nil, views: profileViews))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[profileContentView]|", options: [], metrics: nil, views: profileViews))
        
        
        profileContentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[layerYellow]|", options: [], metrics: nil, views: profileViews))
        profileContentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[tableViewProfile]|", options: [], metrics: nil, views: profileViews))
        profileContentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[layerYellow]-[tableViewProfile]|", options: [], metrics: nil, views: profileViews))
    }
    
    fileprivate func tableView() {
        tableViewProfile.delegate = self
        tableViewProfile.dataSource = self
        tableViewProfile.isScrollEnabled = false
        tableViewProfile.separatorStyle = .none
        tableViewProfile.register(ProfileHeaderCell.self, forCellReuseIdentifier: profileHeaderCell)
        tableViewProfile.register(ProfileCell.self, forCellReuseIdentifier: profileCell)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "PROFILE"
        
        profileSetup(width: view.frame.size.width)
        
        tableView()
        
    }
    
}

extension ProfileViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let headerCell = tableView.dequeueReusableCell(withIdentifier: profileHeaderCell, for: indexPath) as! ProfileHeaderCell
            headerCell.setDataForProfile(strImg: "ic_my", strName: nameHeader[indexPath.row], strBirth: dateOfBirth[indexPath.row])
            return headerCell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: profileCell, for: indexPath) as! ProfileCell
        cell.setDataForProfile(strTitle: titleProfile[indexPath.row], strDetail: detailProfile[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 150
        }
        return 70
    }
    
}
class ProfileHeaderCell : UITableViewCell{
    let stvProfileHeader: UIStackView = {
        let stv = UIStackView()
        stv.translatesAutoresizingMaskIntoConstraints = false
        stv.axis = .vertical
        stv.alignment = .firstBaseline
        stv.distribution = .fillEqually
        stv.spacing = 1
        return stv
    }()
    
    let lblNameHeader: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.font = UIFont.boldSystemFont(ofSize: 21)
        lbl.textAlignment = .center
        lbl.numberOfLines = 1
        return lbl
    }()
    
    let lblBirthHeader: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 18)
        lbl.textAlignment = .center
        lbl.numberOfLines = 1
        return lbl
    }()
    
    let imgHeader : UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.widthAnchor.constraint(equalTo: img.heightAnchor, multiplier: 1).isActive = true
        return img
    }()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .black
//        let layerYellow : UIView = {
//            let layer = UIView()
//            let gradient = CAGradientLayer()
//            gradient.frame = CGRect(x: 0, y: 0, width: 200, height: 2)
//            gradient.colors = [
//                UIColor(red: 0.97, green: 0.91, blue: 0.11, alpha: 0).cgColor,
//                UIColor(red: 0.97, green: 0.91, blue: 0.11, alpha: 1).cgColor,
//                UIColor(red: 0.97, green: 0.91, blue: 0.11, alpha: 0).cgColor
//            ]
//            gradient.locations = [0, 0.5239794075144508, 1]
//            gradient.startPoint = CGPoint(x: 0.98, y: 0.52)
//            gradient.endPoint = CGPoint(x: 0.02, y: 0.52)
//            layer.layer.addSublayer(gradient)
//            return layer
//        }()
        let views = [  "stvProfileHeader":stvProfileHeader, "imgHeader":imgHeader]
        contentView.addSubview(imgHeader)
        contentView.addSubview(stvProfileHeader)
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-30-[imgHeader]-10-[stvProfileHeader]-30-|", options: [], metrics: nil, views: views))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-30-[imgHeader]-30-|", options: [], metrics: nil, views: views))

        stvProfileHeader.heightAnchor.constraint(equalTo: imgHeader.heightAnchor, multiplier: 1).isActive = true
        stvProfileHeader.addArrangedSubview(lblNameHeader)
//        stvProfileHeader.addArrangedSubview(layerYellow)
        stvProfileHeader.addArrangedSubview(lblBirthHeader)
        stvProfileHeader.centerYAnchor.constraint(equalTo: imgHeader.centerYAnchor).isActive = true
    }
    func setDataForProfile(strImg:String?, strName :String?, strBirth:String?) {
        imgHeader.image = UIImage(named: strImg ?? "")
        lblNameHeader.text  = strName ?? ""
        lblBirthHeader.text = strBirth ?? ""
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ProfileCell : UITableViewCell{
    
    let stvProfile: UIStackView = {
        let stv = UIStackView()
        stv.translatesAutoresizingMaskIntoConstraints = false
        stv.axis = .horizontal
        stv.alignment = .fill
        stv.distribution = .fill
        return stv
    }()
    
    let lblTitleProfile: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 18)
        lbl.textAlignment = .left
        lbl.numberOfLines = 1
        return lbl
    }()
    
    let lblDetailProfile: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 18)
        lbl.textAlignment = .right
        lbl.numberOfLines = 1
        return lbl
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .black
        
        let views = ["stvProfile":stvProfile]
        
        contentView.addSubview(stvProfile)
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[stvProfile]-16-|", options: [], metrics: nil, views: views))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[stvProfile]-8-|", options: [], metrics: nil, views: views))
        stvProfile.addArrangedSubview(lblTitleProfile)
        stvProfile.addArrangedSubview(lblDetailProfile)
    }
    func setDataForProfile(strTitle :String?, strDetail:String?) {
        lblTitleProfile.text  = strTitle ?? ""
        lblDetailProfile.text = strDetail ?? ""
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
