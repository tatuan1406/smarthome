//
//  ViewController.swift
//  TabbarApp
//
//  Created by Pavel Bogart on 21/02/2018.
//  Copyright © 2018 Pavel Bogart. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.barTintColor = UIColor.black
        tabBar.tintColor = UIColor.yellow
        tabBar.isTranslucent = false
        setupTabBar()
    }
    
    func setupTabBar() {
        
        let homeViewController = createNavController(vc: HomeViewController(), selected: #imageLiteral(resourceName: "ic_home"), unselected: #imageLiteral(resourceName: "ic_home_selected"))
        let deviceViewController = createNavController(vc: DeviceViewController(), selected: #imageLiteral(resourceName: "ic_device"), unselected: #imageLiteral(resourceName: "ic_device_selected"))
         let profileViewController = createNavController(vc: ProfileViewController(), selected: #imageLiteral(resourceName: "ic_my"), unselected: #imageLiteral(resourceName: "ic_my_selected"))
        
        viewControllers = [homeViewController, deviceViewController, profileViewController]
        
        guard let items = tabBar.items else { return }
        
        for item in items {
            item.imageInsets = UIEdgeInsetsMake(4, 0, -4, 0)
        }
    }
}

extension UITabBarController {
    
    func createNavController(vc: UIViewController, selected: UIImage, unselected: UIImage) -> UINavigationController {
        let viewController = vc
        let navController = UINavigationController(rootViewController: viewController)
        navController.tabBarItem.image = unselected
        navController.tabBarItem.selectedImage = selected
        return navController
    }
}

