//
//  DeviceViewController.swift
//  SmartHomeiOS
//
//  Created by Tinh Van on 4/18/19.
//  Copyright © 2019 hiup. All rights reserved.
//

import UIKit

class DeviceViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "DEVICE"
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.tintColor = .white
        
        view.backgroundColor = .black
    }
    
    
}
