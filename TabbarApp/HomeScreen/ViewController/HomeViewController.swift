//
//  HomeViewController.swift
//  SmartHomeiOS
//
//  Created by Tinh Van on 4/18/19.
//  Copyright © 2019 hiup. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    let homeContentView :UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    let stvTemp:UIStackView = {
        let stv = UIStackView()
        stv.translatesAutoresizingMaskIntoConstraints = false
        stv.axis = .vertical
        stv.alignment = .center
        stv.distribution = .fill
        stv.spacing = 10
        return stv
    }()
    
    let lblTempTitle :UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "Today 's temperature"
        lbl.font = UIFont.systemFont(ofSize: 25)
        lbl.textColor = .white
        return lbl
    }()
    
    let lblTemp :UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "27"
        lbl.font = UIFont.boldSystemFont(ofSize: 50)
        lbl.textColor = .white
        return lbl
    }()
    let lblTempStatus :UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "Status: RAIN"
        lbl.font = UIFont.systemFont(ofSize: 25)
        lbl.textColor = .white
        return lbl
    }()
    
    let menuView :UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let collectionMenu : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 10
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.isScrollEnabled = false
        collection.translatesAutoresizingMaskIntoConstraints = false
        
        return collection
    }()
    
    let menuItemID = "menuItemID"
    

    
    let imgName = ["ic_leavehome", "ic_comehome", "ic_wakeup", "ic_gotobed"]
    let lblName = ["LEAVE HOME", "COME HOME", "WAKEUP", "GO TO BED"]
    
    fileprivate func setupHomeView(width: CGFloat) {
        view.backgroundColor = .black
        
        let layerYellow : UIView = {
            let layer = UIView()
            let gradient = CAGradientLayer()
            gradient.frame = CGRect(x: 0, y: 0, width: width, height: 2)
            gradient.colors = [
                UIColor(red: 0.97, green: 0.91, blue: 0.11, alpha: 0).cgColor,
                UIColor(red: 0.97, green: 0.91, blue: 0.11, alpha: 1).cgColor,
                UIColor(red: 0.97, green: 0.91, blue: 0.11, alpha: 0).cgColor
            ]
            gradient.locations = [0, 0.5239794075144508, 1]
            gradient.startPoint = CGPoint(x: 0.98, y: 0.52)
            gradient.endPoint = CGPoint(x: 0.02, y: 0.52)
            layer.layer.addSublayer(gradient)
            return layer
        }()
        
        let homeViews = ["layerYellow":layerYellow, "homeContentView":homeContentView,"stvTemp":stvTemp,  "menuView":menuView, "collectionMenu":collectionMenu]
        
        //register view
        view.addSubview(homeContentView)
        homeContentView.addSubview(layerYellow)
        homeContentView.addSubview(stvTemp)
        stvTemp.addArrangedSubview(lblTempTitle)
        stvTemp.addArrangedSubview(lblTemp)
        stvTemp.addArrangedSubview(lblTempStatus)
        homeContentView.addSubview(menuView)
        menuView.addSubview(collectionMenu)

        //add constraint for view
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[homeContentView]|", options: [], metrics: nil, views: homeViews))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[homeContentView]|", options: [], metrics: nil, views: homeViews))
        //
        homeContentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[stvTemp]|", options: [], metrics: nil, views: homeViews))
        homeContentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[menuView]|", options: [], metrics: nil, views: homeViews))
        homeContentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[layerYellow]|", options: [], metrics: nil, views: homeViews))
        homeContentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[layerYellow]-16-[stvTemp]-8-[menuView]-16-|", options: [], metrics: nil, views: homeViews))
        menuView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[collectionMenu]|", options: [], metrics: nil, views: homeViews))
        menuView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[collectionMenu]|", options: [], metrics: nil, views: homeViews))
    }
    
    fileprivate func setupNavigation(){
        navigationController?.navigationBar.barStyle = .blackOpaque
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Hi, Ta Tuan"
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_notif"), style: .plain, target: self, action: #selector(notificationScreen))
    }
    
    fileprivate func setupCollection() {
        collectionMenu.delegate = self
        collectionMenu.dataSource = self
        collectionMenu.register(menuItemCell.self, forCellWithReuseIdentifier: menuItemID)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigation()
        setupHomeView(width: view.frame.size.width)
        setupCollection()
    }
    
    //
    @objc fileprivate func notificationScreen(){
        let notifVC = NotificationTableViewController()
        
        self.navigationController?.pushViewController(notifVC, animated: true)
    }
    
    //set status bar style
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
extension HomeViewController:UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let menuCell = collectionView.dequeueReusableCell(withReuseIdentifier: menuItemID, for: indexPath) as! menuItemCell
        menuCell.setDataForMenu(img: imgName[indexPath.row], strMenu: lblName[indexPath.row])
        return menuCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  30
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2 , height: collectionView.frame.size.width / 2)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(10, 10, 10, 10)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
    }
}
class menuItemCell : UICollectionViewCell{
    
    let item_view : UIView = {
        let myView = UIView()
        myView.cardView()
        myView.translatesAutoresizingMaskIntoConstraints = false
        return myView
    }()
    
    let imgMenu: UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let lblMenu : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.textColor = .white
        lbl.textAlignment = .center
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(item_view)
        item_view.addSubview(imgMenu)
        item_view.addSubview(lblMenu)
        
        //dictionary of view
        let views = ["item_view" : item_view, "imgMenu" : imgMenu, "lblMenu" : lblMenu]
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-24-[item_view]-24-|", options: [], metrics: nil, views: views))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-24-[item_view]-24-|", options: [], metrics: nil, views: views))
        
        item_view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[imgMenu]-16-|", options: [], metrics: nil, views: views))
        item_view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[lblMenu]-8-|", options: [], metrics: nil, views: views))
        
        item_view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[imgMenu]-8-[lblMenu(25)]-8-|", options: [], metrics: nil, views: views))
    }
    
    func setDataForMenu(img: String? , strMenu:String?){
        imgMenu.image  = UIImage(named: img ?? "")
        lblMenu.text = strMenu
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
