//
//  NotificationTableViewController.swift
//  TabbarApp
//
//  Created by Tinh Van on 4/19/19.
//  Copyright © 2019 Pavel Bogart. All rights reserved.
//

import UIKit

class NotificationTableViewController: UITableViewController {

    let notifCellID = "notifCellID"
    let strNotif = ["1234567890", "qwertyuio", "SDFASDFASDFASDF","FASDFQWEQRQWFSDASVZXCVXCVZXCVZXHCVHJSDGFHJSGKDFGASJK","SADFASFASFDASDFASDFAFWASDFASDFFASDSDFASFD","XVCZZXVCVZXCZXCVZXCVZXCVZXCVZXCV","ASFDSDAFASFDSDFASDFASADFSDFASADFSADSADFASDFSDFAASDF","QWREWERWRWERQQWERQWERWQERWQERWERQ","245262424324352345234523452345234523452345234523452345","ERYTREYTRETYRETYERTYERTYERTYRETYRETY"]
    override func viewDidLoad() {
        super.viewDidLoad()

        
        navigationItem.title = "NOTIFICATION"
        
        tableView.backgroundColor = .black
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.estimatedRowHeight = 75
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.register(NotificationCell.self, forCellReuseIdentifier: notifCellID)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return strNotif.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: notifCellID , for: indexPath) as! NotificationCell
        cell.setDataNotif(strNotif: strNotif[indexPath.row])
        return cell
    }
    
}

class NotificationCell : UITableViewCell{
    
    let imgNotif : UIImageView = {
       let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.image = UIImage(named: "ic_my_selected")
        return img
    }()
    
    let lblNotif : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: 22)
        lbl.numberOfLines = 0
        lbl.textColor = .white
        return lbl
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .black
        let views = ["imgNotif" : imgNotif , "lblNotif": lblNotif]
        contentView.addSubview(imgNotif)
        contentView.addSubview(lblNotif)
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[imgNotif]-16-[lblNotif]-8-|", options: [], metrics: nil, views: views))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[lblNotif]-8-|", options: [], metrics: nil, views: views))
        
        imgNotif.centerYAnchor.constraint(equalTo: lblNotif.centerYAnchor).isActive = true
        imgNotif.widthAnchor.constraint(equalTo: imgNotif.heightAnchor, multiplier: 1).isActive = true
    }
    func setDataNotif(strNotif:String? ){
        lblNotif.text = strNotif ?? ""
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
