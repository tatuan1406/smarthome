//
//  CardView.swift
//  TabbarApp
//
//  Created by Tinh Van on 4/19/19.
//  Copyright © 2019 Pavel Bogart. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable

extension UIView{
    func cardView()  {
        self.layer.cornerRadius = 10.0
//        self.layer.shadowColor = UIColor.white.cgColor
        self.layer.borderColor = UIColor.deepYellow.cgColor
                self.layer.borderWidth = 2
        
//        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//        self.layer.shadowOpacity = 0.4
    }
    
}
